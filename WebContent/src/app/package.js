var profile = (function(){
    return {
        resourceTags: {
        	test: function (filename, mid) {
    			return false;
    		},
    		copyOnly: function (filename, mid) {
    			return (/^app\/resources\//.test(mid) && !/\.css$/.test(filename));
    		},
            amd: function(filename, mid) {
                return /\.js$/.test(filename);
            },
            miniExclude: function (filename, mid) {
    			return mid in {
    				'app/package': 1
    			};
    		}
        }
    };
})();