define(["dijit/registry","dijit/layout/BorderContainer",
         "dijit/layout/TabContainer","dijit/layout/ContentPane",
         "dojo/_base/config","dijit/Dialog",
         "dojo/json","dojo/domReady!"], 
         function(registry,BorderContainer,TabContainer,ContentPane,config,Dialog,JSON) {
            //Create Main App Layout - BorderContainer
            var appLayout = new BorderContainer({
               design:"headline" ,
            },"appLayout");
            
            //Create TabContainer - TabLayout inside border container
            var contentTabs = new TabContainer({
               region:"center",
               id: "contentTabs",
               tabPosition: "bottom",
               "class":"centerPanel"
            });
            
            //add Tab child to Border Parent
            appLayout.addChild(contentTabs);
            
            
            //Create Content Layouts
            appLayout.addChild(
                new ContentPane({
                    region: "top",
                    "class": "edgePanel",
                    content: "Header content(top)"
                })
            );
            
            appLayout.addChild(
                new ContentPane({
                    region:"left",
                    id:"leftCol",
                    "class":"edgePanel",
                    content: "Sidebar content (left)",
                    splitter: true
                    
                })
            );
            
            //add content to tabContainer
            var tabContent = new ContentPane({
                innerHTML: "<h2>This is TAB 1",
                title: "Tab 1"
            }); 
            
           
            contentTabs.addChild(tabContent);
            
            contentTabs.addChild(
                new ContentPane({
                    innerHTML: "<h2>This is TAB 2",
                    title: "Tab 2"
                })
            );
            
            var dialog = new Dialog({
                title: "Welcome back " + config.app.userName,
                content: "Jarvis is the matter"
            });

            // Draw on the app config to put up a personalized message
            dialog.show();
            
            //Start-up and doLayout
            appLayout.startup();
            
		});